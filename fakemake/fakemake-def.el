;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'akater-sh
  authors "Dima Akater"
  first-publication-year-as-string "2019"
  org-files-in-order '("akater-sh-env"
                       "akater-sh-wrap"
                       "akater-sh")
  org-files-for-testing-in-order '("akater-sh-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-prepare :before
            (lambda ()
              (warn "This package is obsolete.  Use shmu instead: https://framagit.org/akater/elisp-shmu")))
